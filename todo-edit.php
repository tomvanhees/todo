<?php
    require_once('bootstrap.php');

    if ($_POST) {
        $query->update("todos", [
            "date"  => $_POST['date'],
            "title" => $_POST['title'],
            "note"  => $_POST['note'],
            "id"    => $_POST['id']
        ]);

    }

    $todo = $query->getById('todos', $_GET['id']);
    $title = "Bewerk je To Do:";
    $action = "todo-edit.php?id=" . $_GET['id'];
    include("views/todo-form-view.php");
