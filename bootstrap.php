<?php

    require_once ('database/Connection.php');
    require_once ('database/Querybuilder.php');
    require_once ('classes/Todo.php');

    $pdo = Connection::make();
    $query = new Querybuilder($pdo);