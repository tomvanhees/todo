<?php

    class Querybuilder
    {
        public $pdo;

        public function __construct($pdo)
        {
            $this->pdo = $pdo;
        }

        public function getAllOrder($table, $column, $order)
        {
            $classname = substr(ucfirst($table), 0, -1);
            $statement = $this->pdo->prepare("select * from {$table} ORDER by {$column} {$order}");
            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_CLASS, $classname);
        }

        public function getById($table, $id)
        {
            $classname = substr(ucfirst($table), 0, -1);
            $sql = sprintf("SELECT * FROM %s WHERE id = :id",
                $table);
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute([

                    "id" => $id
                ]);

            } catch (PDOException $e) {
                die($e->getMessage());
            }

            return $stmt->fetchAll(PDO::FETCH_CLASS, $classname)[0];

        }

        public function toggle($table, $column, $id)
        {
            $sql = sprintf("UPDATE %s SET {$column}= !{$column} WHERE id = %s",
                $table,
                $id);
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute();

            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }

        public function delete($table, $id)
        {
            $sql = sprintf("DELETE FROM %s WHERE id = %s",
                $table,
                $id);
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute();

            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }

        public function update($table, $parameters)
        {
            $id = array_pop($parameters);
            $set = "";

            foreach ($parameters as $key => $value) {
                $set .= $key . "= :" . $key . ",";
            }
            $set = substr($set, 0, -1);
            $sql = sprintf('UPDATE %s SET %s WHERE id = %s',
                $table,
                $set,
                $id);
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($parameters);
            } catch (PDOException $e) {
            }

        }
        public function insert($table, $parameters){
            $sql= sprintf('insert into %s(%s) values (%s)',
                $table,
                implode(', ', array_keys($parameters)),
                ':'.implode(', :',array_keys($parameters)));
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($parameters);
            }
            catch(PDOException $e){
                die($e->getMessage());
            }
        }




    }