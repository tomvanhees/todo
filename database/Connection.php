<?php

    class Connection
    {
        public static function make()
        {
            try{
                $pdo = new PDO("mysql:host=localhost;dbname=todo","root","");
                $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                return $pdo;
            }catch(PDOException $e){
                die($e->getMessage());
            }
        }
    }