<?php
    require_once ('bootstrap.php');

    if($_POST){
        $query->insert('todos',[
            "date" => $_POST['date'],
            "title" => $_POST["title"],
            "note" => $_POST['note'],
            "status" => 0
        ]);
        header('location: index.php');
        exit;
    }

    $title = "Voeg een To Do Toe:";
    $action = "todo-voegtoe.php";
include("views/todo-form-view.php");