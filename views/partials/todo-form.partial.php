<div class="container mx-auto flex flex-wrap justify-center mt-8 ">
    <h1 class="w-full text-center mb-8"><?= $title ?></h1>
    <form action="<?= $action ?>" method="post" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 bg-blue-lightest">
        <input type="text" name="id" hidden value="<?= isset($todo->id) ? $todo->id : "" ?> ">
        <p class="mt-2 mb-2"><label for="date" class="font-bold block ml-2">Datum: </label><input type="date" id="date"
                                                                                             name="date"
                                                                                             class="shadow border rounded bg-grey-lighter w-full"
                                                                                             value="<?= isset($todo->date) ? $todo->date : "" ?>">
        </p>
        <p class="mt-2 mb-2"><label for="titel" class="font-bold block ml-2">Titel: </label><input type="text" id="titel"
                                                                                              name="title"
                                                                                              class="shadow border rounded bg-grey-lighter w-full"
                                                                                              value="<?= isset($todo->title) ? $todo->title : "" ?>">
        </p>
        <p class="mt-2 mb-2"><label for="note" class="font-bold block mb-2">Nota:</label>
            <textarea name="note" id="note" cols="30" rows="10"
                      class="shadow border rounded bg-grey-lighter"><?= isset($todo->note) ? $todo->note : "" ?></textarea>
        </p>
        <p class="mt-2 mb-2">
            <button name="verzendtodo" class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded">
                Verzenden
            </button>
        </p>

    </form>
    <div class="w-full text-center">
        <a href="index.php" class="btn text-white font-bold py-2 px-4 rounded bg-green hover:bg-green-dark">Terug</a>
    </div>
</div>

