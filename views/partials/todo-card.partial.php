<div class="max-w-sm rounded overflow-hidden schadow-lg w-1/3 border bg-grey-lightest">
    <div class="px-6 py-4 flex flex-row-reverse justify-between">
        <div class="flex flex-col">
            <?php if ($todo->status == 0): ?>
                <a href="todo-edit.php?id=<?= $todo->id ?>"
                   class="btn bg-blue hover:bg-blue-dark text-white font-bold py-4 px-4 rounded">Bewerk</a>
            <?php endif; ?>

            <a href="verwijder.php?id=<?= $todo->id ?>"
               class="btn bg-red hover:bg-red-dark text-white font-bold py-4 px-4 rounded">Verwijder</a>
            <a href="toggle.php?id=<?= $todo->id ?>"
               class="btn bg-green<?= $todo->status == 0 ? '' : '-dark' ?> hover:bg-green<?= $todo->status == 1 ? '' : '-dark' ?> text-white font-bold py-4  px-4 rounded"><?= $todo->status == 0 ? 'Voltooi' : 'Activeer' ?> </a>
        </div>
        <div>

        <div class="font-bold text-xl mb-2 "><?= $todo->date ?></div>
            <div class=" <?= $deadline < 5 ? 'text-red-dark' : 'text-green-dark' ?> mb-3">Aantal dagen over: <?= $deadline ?></div>
        <div class="<?= $todo->status == 1 ? 'line-through' : '' ?>"><span
                    class="font-bold mb-2 ">Titel: </span><?= $todo->title ?></div>
        <div class="<?= $todo->status == 1 ? 'line-through' : '' ?> clearfix"><span class="font-bold mb-2 ">Omschrijving: </span><br><?= $todo->note ?>
        </div>
    </div>

    </div>
</div>