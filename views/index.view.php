<?php include('views/partials/start.partial.php'); ?>


<div class="container mx-auto flex justify-center items-center mt-8">
    <a href="todo-voegtoe.php" class="btn bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded">Voeg items toe</a>
</div>

<div class="container mx-auto flex justify-center items-center mt-8">
    <div class="flex flex-wrap mb-4">

        <?php foreach ($todos as $todo): ?>
        <?php $deadline = $todo->getDeadline($today) ?>
            <?php include ('views/partials/todo-card.partial.php') ?>
        <?php endforeach;?>

    </div>
    </div>
</div>


<?php include('views/partials/end.partial.php'); ?>

