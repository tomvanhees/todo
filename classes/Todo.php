<?php

     class Todo{
         public $id;
         public $date;
         public $titel;
         public $note;
         public $status;

         public function getDeadline($today){

            return round((strtotime($this->date) - strtotime($today))/(60*60*24));
         }
     }